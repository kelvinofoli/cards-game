import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import LandingPage from '../screens/landingPage/landingPage';
import Home from '../screens/home/home';
import Settings from '../screens/settings/settings';
import Profile from '../screens/profile/profile';
import Store from '../screens/store/store';
import StartGame from '../screens/startGame/startGame';
import WithdrawCoins from '../screens/withdrawCoins/withdrawCoins';
import Gameplay from '../screens/gameplay/gameplay';
import Game from '../screens/gameplay/game/game';

// const homebtn = (props)=> <TouchableOpacity onPress={this.props.navigation.navigate('Home')}> 
// <View style={styles.container}>
// <FontAwesome5 name={'home'} size={30} color="#ffb14c" />
//      </View>
// </TouchableOpacity>
const globalStyles = require('../styles/style')

const Stack = createStackNavigator();
const config = {
    animation: 'spring',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };
const Routes = () => {
    return (
        <NavigationContainer initialRouteName ="LandingPage">
            <Stack.Navigator >
                <Stack.Screen
                    name="LandingPage"
                    component={LandingPage}
                    options= {{headerShown:false }}
                />
                <Stack.Screen
                    name="Home"
                    component={Home}
                    options= {{
                        headerShown:false,
                        animationEnabled:false
                    }}
                />
                <Stack.Screen
                    name="Settings"
                    component={Settings}
                    options= {{
                        title: 'Settings',
                        animationEnabled:false,
                        headerTitleAlign:'center',
                        headerStyle: globalStyles.headerStyles,
                        cardShadowEnabled: false,
                        headerTitleStyle:{color: 'white',fontSize: 30},
                        headerTintColor:'white'

                    }}
                />
                <Stack.Screen
                    name="Profile"
                    component={Profile}
                    options= {{
                        title: 'Profile',
                        animationEnabled:false,
                        headerTitleAlign:'center',
                        headerStyle: globalStyles.headerStyles,
                        cardShadowEnabled: false,
                        headerTitleStyle:{color: 'white',fontSize: 30},
                        headerTintColor:'white'

                    }}
                />
                <Stack.Screen
                    name="Store"
                    component={Store}
                    options= {{
                        title: 'Store',
                        animationEnabled:false,
                        headerTitleAlign:'center',
                        headerStyle: globalStyles.headerStyles,
                        cardShadowEnabled: false,
                        headerTitleStyle:{color: 'white',fontSize: 30},
                        headerTintColor:'white'
                    }}
                />
                <Stack.Screen
                    name="StartGame"
                    component={StartGame}
                    options= {{
                        title: 'Select Game Mode',
                        headerTitleStyle:{color: 'white',fontSize: 25},
                        headerTitleAlign:'center',
                        headerTintColor:'white',
                        headerStyle: {
                            backgroundColor: '#F25C05',
                            shadowOpacity: 0,
                            elevation: 0,

                        },
                        cardShadowEnabled: false,
                        animationEnabled:false,
                        // headerTransparent: true
                    }}
                />
                <Stack.Screen
                    name="WithdrawCoins"
                    component={WithdrawCoins}
                    options= {{
                        title: 'Withdraw Coins',
                        animationEnabled:false,
                        headerTitleAlign:'center',
                        headerStyle: [globalStyles.headerStyles,{backgroundColor:'#119300'}],
                        cardShadowEnabled: false,
                        headerTitleStyle:{color: 'white',fontSize: 30},
                        headerTintColor:'white'
                    }}
                />
                <Stack.Screen
                    name="Gameplay"
                    component={Gameplay}
                    options= {{
                        headerShown:false,
                        animationEnabled:false
                    }}
                />
                <Stack.Screen
                    name="Game"
                    component={Game}
                    options= {{
                        headerShown:false,
                        animationEnabled:false
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
};
export default Routes;