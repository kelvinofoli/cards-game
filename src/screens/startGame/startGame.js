import React from 'react'
import { View, Text, StyleSheet ,TouchableHighlight} from 'react-native'


const globalStyles = require('../../styles/style')

export default function StartGame(props) {
    return (
        <View style={[globalStyles.container, {alignItems:'center',justifyContent:'space-around'}]}>
            <View style={styles.mode} onTouchEnd={() => props.navigation.navigate('Gameplay',{mode: 'singleplayer'})}>
                <Text style={styles.text}>Single Player</Text>
            </View>
            <View style={styles.mode} onTouchEnd={() => props.navigation.navigate('Gameplay',{mode: 'multiplayer'})}>
                <Text style={styles.text}>Multiplayer</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    text:{
        fontSize: 20,
        color:'white'
    },
    mode: {
        width: '90%',
        height: '45%',
        opacity: 0.8,
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F25C05',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 7.49,
        elevation: 12,
    }
})