import React, { Component } from 'react'
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native'

import Game from './game/game'

const globalStyles = require('../../styles/style')

export default class Gameplay extends Component {
    state = { animating: true }

    closeActivityIndicator = () => setTimeout(() => this.setState({
        animating: false
    }), 60000)

    componentDidMount = () => this.closeActivityIndicator()
    render() {
        const animating = this.state.animating

        const { mode } = this.props.route.params

        if (mode === 'singleplayer') {
            return (
               <Game mode={mode} navigate={this.props.navigation.navigate}/>
            )
        } else {
            return (
                <View style={[globalStyles.container, { justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }]}>
                    <View style={styles.card}>
                        <Text >Waiting for players</Text>
                        <ActivityIndicator
                            animating={animating}
                            color='#bc2b78'
                            size="large"
                            style={styles.activityIndicator} />
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 20,
        paddingLeft: 35,
        paddingRight: 35,
        justifyContent:'space-between'
    },
    top: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    card: {
        backgroundColor: '#ccc',
        width: 200,
        height: 200,
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 20,
        padding: 20
    },
    activityIndicator: {
        //    flex: 1,
        //    justifyContent: 'center',
        //    alignItems: 'center',
        height: 70
    }

})