import React, {useState} from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { SmallButton, LargeButton } from '../../../components/buttons'

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function PauseGame(props) {

    const [audioState,setAudioState] = useState(true)
    const [musicState,setMusicState] = useState(true)



    const audioOff = () =>{


        setAudioState(!audioState)
    }
    const musicOff = () =>{
        
        setMusicState(!musicState)
    }
    
    let audioBtn = (<FontAwesome5 name="volume-up" size={20} color="white"/>)
    let musicBtn = (<FontAwesome5 name="music" size={20} color="white"/>)

    if(audioState){
        audioBtn = (<FontAwesome5 name="volume-up" size={20} color="white"/>)
    }else{
        audioBtn = (<FontAwesome5 name="volume-mute" size={20} color="white"/>)
    }

    if(musicState){
        musicBtn = (<FontAwesome5 name="music" size={20} color="white"/>)
    }else{
        musicBtn = (<FontAwesome5 name="ban" size={20} color="white"/>)
    }
    
    return (
        <View style={styles.card}>
            <Text style={[styles.txt,{fontWeight:'600'}]}>Paused</Text>
            <View style={{ width: '100%', flexDirection: 'row',justifyContent:'space-evenly' }}>

                <View style={styles.soundBtn} onTouchEnd={() => audioOff()}>
                    {audioBtn}
                </View>
                <View style={styles.soundBtn} onTouchEnd={() => musicOff()}>
                    {musicBtn}
                </View>

            </View>
            <View style={styles.button} onTouchEnd={()=>props.pause()}><Text style={styles.txt}>Resume</Text></View>
            <View style={styles.button}><Text style={styles.txt}>Restart</Text></View>
            <View style={styles.button} onTouchEnd={()=>props.navigate('Home')}><Text style={styles.txt}>Exit</Text></View>
        </View>
    )
}

const styles = StyleSheet.create({
    soundBtn: { width: 50, height: 50, borderRadius: 25, backgroundColor: '#ffa775' ,alignItems:'center',justifyContent:'center'},
    card: {
        backgroundColor: '#FFCAAB',
        width: 250,
        height: 320,
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 40,
        padding: 20,
        marginBottom:50
    },
    button: {
        borderWidth: 1,
        borderColor: 'white',
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderRadius: 25,
        backgroundColor: null
    },
    txt: {
        fontSize: 22,
        color: 'white'
    }
})