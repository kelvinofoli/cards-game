import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

export default function WonGame() {
    return (
        <View style={styles.card}>
            <Text style={{color:'#FFE006',fontSize: 20}}>You Won!!!</Text>
            <Image
                source={require('../../../assets/comp_1.gif')}
                style={{ width: 150, height: 150 }}
            />
            <View style={styles.button}><Text style={styles.txt}>Play Again</Text></View>
            <View style={styles.button}><Text style={styles.txt}>Go Home</Text></View>
        </View>
    )
}

const styles = StyleSheet.create({
    txt: {
        color: "black",
        fontSize: 22,

    },
    card: {
        backgroundColor: '#F0FAFC',
        width: 270,
        height: 350,
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 40,
        padding: 20,
        marginBottom: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 7.49,

        elevation: 12,
    },
    button: {
        borderWidth: 1,
        borderColor: 'black',
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderRadius: 25,
        backgroundColor: null
    },
})
