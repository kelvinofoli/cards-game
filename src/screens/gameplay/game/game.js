import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet ,Dimensions} from 'react-native'
import { SmallButton, LargeButton } from '../../../components/buttons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


//Game State Cards
import WonGame from '../wonGame/wonGame'
import PauseGame from '../pauseGame/pauseGame'
import LostGame from '../lostGame/lostGame'

import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient'

const globalStyles = require('../../../styles/style')

const {width,height}= Dimensions.get('screen');

export default function Game(props) {

    //PAUSE , WON And LOST States
    const [victoryState, setVictoryState] = useState(false)
    const [pauseState, setPauseState] = useState(false)



    const opponent = (<View style={styles.opponent}><FontAwesome5 name={'user'} size={20} color="white" /><Text>#Player</Text></View>);



    let MAINGAMESTATE;



    if (pauseState) {
        MAINGAMESTATE = (<PauseGame pause={() => setPauseState(!pauseState)} navigate={props.navigate} />);
    } else {
        MAINGAMESTATE = (
        <View style={{ width: '100%', height: '100%'}}>
            <Text style={globalStyles.text}>{'\n'}Lorem{'\n'}Ipsum{'\n'}</Text>

            {props.mode === 'multiplayer' ? opponent : null}

            <Text style={{ textAlign: 'center' }}>Guess the right card</Text>

            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20 }}>
                <View style={styles.gameBox}>
                    <AnimatedLinearGradient style={styles.gameBox} customColors={presetColors.instagram} speed={1000}>
                        <Text style={styles.boxText}>?</Text>
                    </AnimatedLinearGradient>
                </View>
                {/* <View style={styles.gameBox}>
                    <AnimatedLinearGradient style={styles.gameBox} customColors={presetColors.instagram} speed={4000}><Text style={styles.boxText}>?</Text>
                    </AnimatedLinearGradient>
                </View> */}
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10}}>
                <View style={styles.gameBox}>
                    <AnimatedLinearGradient style={styles.gameBox} customColors={presetColors.sunrise} speed={4000}><Text style={styles.boxText}>?</Text></AnimatedLinearGradient>
                </View>
                <View style={styles.gameBox}>
                    <AnimatedLinearGradient style={styles.gameBox} customColors={presetColors.firefox} speed={4000}><Text style={styles.boxText}>?</Text></AnimatedLinearGradient>
                </View>
            </View>
        </View>
        );
    }




    // const pauseGame = () => {MAINGAMESTATE = (<PauseGame/>)} 



    return (
        <View style={styles.container}>
            <View style={styles.top} >
                <View style={styles.pauseBtn} onTouchEnd={() => setPauseState(!pauseState)}>
                    {/* <Text style={[globalStyles.text, { color: 'white' }]}>II</Text> */}
                    <FontAwesome5 name={pauseState ? "play" : "pause"} size={25} color="white" />
                </View>
                <View style={{ backgroundColor: '#F25C05', width: 75, height: 30, flexDirection: 'row', alignItems: 'center', borderRadius: 40, justifyContent: 'space-around', paddingLeft: 10, paddingRight: 10 }}>
                    <FontAwesome5 name={'coins'} size={17} color="#FFBB00" />
                    <Text style={[globalStyles.text, { color: '#FFBB00' }]}>100</Text>
                </View>
            </View>
<View style={styles.MAINGAMESTATE}>{MAINGAMESTATE}</View>
                
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 20,
        paddingLeft: '8%',
        paddingRight: '8%',
        justifyContent: 'space-between',
        ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: '100%',
    },
    MAINGAMESTATE: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%'
    },

    pauseBtn: {
        width: 50,
        height: 50,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#8EC63F',
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 6,
        // },
        // shadowOpacity: 0.2,
        // shadowRadius: 7.49,

        // elevation: 12,


    },


    opponent: {
        backgroundColor: 'orange',
        flexDirection: 'row',
        width: '40%',
        height: 40,
        alignItems: 'center',
        padding: 20,
        justifyContent: 'space-between',
        borderRadius: 10,
        marginBottom: 15
    },
    top: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    gameBox: {
        width: width*0.38,
        height: width*0.38,
        backgroundColor: '#ccc',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxText: {
        color: 'white',
        fontSize: 30,
        textAlign: 'center'
    }
})