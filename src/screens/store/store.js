import React from 'react'
import { View, Text, StyleSheet,ScrollView } from 'react-native'
import Avatar from '../../components/avatar'

const globalStyles = require('../../styles/style')

const avatar = [
    require('../../assets/avatars/1.png'),
    // require('../../assets/avatars/2.png'),
    // require('../../assets/avatars/3.jpg'),
]

export default function Store(props) {
    return (
        <View style={[globalStyles.container, styles.container]}>
            <View style={styles.outerbox}>
                <Text style={[globalStyles.text,{color:'white'}]}>Coins</Text>
                <View style={[styles.box,{backgroundColor:'#ffb266'}]}>
                <View style={styles.row}></View>
                <View style={[styles.row,{backgroundColor:'#ffcfa0'}]}></View>
                <View style={styles.row}></View>
                </View>
            </View>
            <View style={[styles.outerbox, { height:'27%'}]}> 
                <Text style={[globalStyles.text,{color:'white'}]}>Avatars</Text>
                <View style={[styles.box,{padding:20,paddingLeft:0,paddingRight:0 }]}>
                    <ScrollView horizontal={true} style={{width:'100%', flexDirection:'row'}} contentContainerStyle={{alignItems:'center'}}>
                        {/* {avatar.map((a)=> <Avatar source={a}/>)} */}
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                        <Avatar source={avatar[1]}/>
                    </ScrollView>
                </View>
            </View>

        </View>
    )
}
const styles = StyleSheet.create({
    outerbox: {
        width: '100%',
        height: '50%',
        // borderWidth:1,
        alignItems:'center',
        marginBottom: 50
    },
    container: {
        // alignItems: 'center',
    },
    box: {
        backgroundColor: 'white',
        borderRadius: 35,
        height: '100%',
        width: '90%',
        justifyContent: 'space-around',
        alignItems: 'center',
        // padding: 20
    },
    row:{
        height:'33.33%',
        width:'100%'
    }

})
