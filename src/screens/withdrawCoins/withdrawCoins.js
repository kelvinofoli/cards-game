import React from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, TextInput } from 'react-native'
import { SwitchButton, SmallButton } from '../../components/buttons'

const globalStyles = require('../../styles/style')


const withDrawalHistory = [
    { Amount: '10', date: '5/9/19', key: '1' },
    { Amount: '30', date: '5/9/19', key: '3' },
    { Amount: '30', date: '5/9/19', key: '4' },
    { Amount: '30', date: '5/9/19', key: '5' },
    { Amount: '30', date: '5/9/19', key: '6' },
    { Amount: '30', date: '5/9/19', key: '7' },
    { Amount: '30', date: '5/9/19', key: '8' },
    { Amount: '30', date: '5/9/19', key: '9' },
    { Amount: '30', date: '5/9/19', key: '10' },
    { Amount: '30', date: '5/9/19', key: '11' },
    { Amount: '30', date: '5/9/19', key: '12' },
]

export default function WithdrawCoins() {
    return (
        <View style={[globalStyles.container, styles.container]}>
            <View style={styles.outerbox}>
                <Text style={[globalStyles.text, { color: 'white' }]}>Cash out</Text>
                <View style={[styles.box, { padding: 12 }]}>
                    <Text style={{ textAlign: 'center' }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</Text>
                    <SmallButton style={{ backgroundColor: 'white', borderRadius: 6, width: 150, elevation: 0, borderWidth: 1, borderColor: 'green' }}><Text>10 coins = xx dollars</Text></SmallButton>
                    <Text style={{ alignSelf: 'flex-start' }}>Coins Available : 100</Text>
                    <TextInput placeholder="AMOUNT" placeholderTextColor="#ccc" style={{ borderWidth: 2, borderRadius: 3, borderColor: '#ccc', width: 150, textAlign: 'center' }} keyboardType="numeric" />
                    <SmallButton style={{backgroundColor: 'green', borderRadius: 6, width: 120, elevation: 0, borderWidth: 1, borderColor: 'green'}} title="Withdraw" />
                </View>
            </View>
            <View style={[styles.outerbox, { height: '27%' }]}>
                <Text style={[globalStyles.text, { color: 'white' }]}>History</Text>
                <View style={[styles.box, { padding: 20, paddingLeft: 0, paddingRight: 0 }]}>
                    <ScrollView snapToStart={true} style={{ width: '100%' }}>
                        <FlatList ListHeaderComponent={<View style={styles.historyItem}><Text style={styles.header}>Amount</Text><Text style={styles.header}>Date</Text></View>}
                            data={withDrawalHistory}
                            renderItem={({ item }) => <View style={styles.historyItem}><Text style={[styles.header, { color: '#000' }]}>{item.Amount}</Text><Text style={[styles.header, { color: '#000' }]}>{item.date}</Text></View>}
                        />
                        {/* <View style={styles.historyItem}><Text>Amount</Text><Text>Date</Text></View>  */}
                    </ScrollView>
                </View>
            </View>

        </View>
    )
}
const styles = StyleSheet.create({
    header: {
        marginLeft: 10,
        marginRight: 10,
        color: '#ccc',
        fontSize: 16
    },
    historyItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    outerbox: {
        width: '100%',
        height: '50%',
        alignItems: 'center',
        marginBottom: 50
    },
    container: {
        backgroundColor: '#18d600'
    },
    box: {
        backgroundColor: 'white',
        borderRadius: 35,
        height: '100%',
        width: '90%',
        justifyContent: 'space-between',
        alignItems: 'center',
        // padding: 20
    },
    row: {
        height: '33.33%',
        width: '100%'
    }

})
