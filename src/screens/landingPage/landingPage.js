import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import {SmallButton} from '../../components/buttons'
// const { width, height } = Dimensions.get("window");

const globalStyles = require('../../styles/style');


export default function LandingPage(props) {
  const navigate = props.navigation.navigate;
  return (
    <View style={[globalStyles.container,styles.container]}>
      <Text style={globalStyles.text}>Lorem{'\n'}Ipsum</Text>

      <View style={styles.carousel}>

      </View>
      <View style={styles.bottom}>
            <SmallButton title={'Skip'} pressed={() => props.navigation.navigate('Home')}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent:'space-between',
    alignItems:'center',
    paddingTop: 80,
  },
  carousel:{
    width: '80%',
    height: 300,
    backgroundColor: '#ff843d',
    alignSelf: "center"
  },
  bottom:{
    alignSelf:'flex-end',
    width:'100%',
    height: 50,
    // backgroundColor:'red',
    justifyContent:'center',
    alignItems:'center',
    // flexDirection: 'row',
  }

})