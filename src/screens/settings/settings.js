import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import {SwitchButton,SmallButton} from '../../components/buttons'
const globalStyles = require('../../styles/style')

export default function Settings(props) {
    return (
        <View style={[globalStyles.container, styles.container]}>
            <View style={styles.box}>
                <View style={[styles.row,{justifyContent: 'center',borderWidth: 5, borderColor:'#ffb14c'}]} onTouchEnd={()=> props.navigation.navigate('Profile')}><Text style={styles.setting}>Profile</Text></View>


                <View style={styles.row}><Text style={styles.setting}>Sounds</Text><SwitchButton/></View>
                <View style={styles.row}><Text style={styles.setting}>Music</Text><SwitchButton/></View>
                <View style={styles.row}><Text style={styles.setting}>Sound Effects</Text><SwitchButton/></View>
                <View style={styles.row}><Text style={styles.setting}>Notifications</Text><SwitchButton/></View>
            </View>
            <SmallButton style={{backgroundColor:'#d35100', width: 150, height: 40, elevation: 5
 }}><Text style={{fontSize:20, color:'white'}}>Help/FAQ</Text></SmallButton>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    box: {
        backgroundColor: 'white',
        borderRadius: 35,
        height: '50%',
        width: '90%',
        justifyContent: 'space-around',
        padding: 20,
        paddingLeft: 30,
        paddingRight: 30
    },
    buttom: {

    },
    row: {
        width: '100%',
        justifyContent: 'space-between',
        flexDirection:'row',
        alignItems:'center'
    },
    setting: {
        fontSize: 20,
        color:'#d35100'
    }
})
