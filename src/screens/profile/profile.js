import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import {SwitchButton,SmallButton} from '../../components/buttons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const globalStyles = require('../../styles/style')

export default function Profile(props) {
    return (
        <View style={[globalStyles.container, styles.container]}>
            <View style={styles.box}>
                <View style={styles.userAvatar}>
                <FontAwesome5 name={'user'} size={30} color="#ffb14c" />

                </View>
                <View style={styles.row}><Text style={styles.text}>Username</Text><Text>dasd</Text></View>
                <View style={styles.row}><Text style={styles.text}>Email</Text><Text>AAA@gmail.com</Text></View>
                <View style={styles.row}><Text style={styles.text}>Change game theme</Text></View>

                <Text>#Themes</Text>
                <SmallButton style={{backgroundColor:null, width: 200, height: 40, elevation: 0,shadowOpacity: 0, borderWidth:2,borderRadius:6,alignSelf:'flex-start'}}><Text style={{fontSize:15, color:'#474747'}}>Change Withdrawal method</Text></SmallButton>
            </View>
            <SmallButton style={{backgroundColor:'#d35100', width: 100, height: 40, elevation: 5}}><Text style={{fontSize:20, color:'white'}}>Save</Text></SmallButton>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    box: {
        backgroundColor: 'white',
        borderRadius: 35,
        height: '50%',
        width: '90%',
        justifyContent: 'space-around',
        alignItems:'center',
        padding: 20,
        paddingLeft: 30,
        paddingRight: 30
    },
    userAvatar:{
        width:80,
        height:80,
        borderRadius:150,
        borderColor:'#ffb14c',
        borderWidth: 4,
        alignItems:'center',
        justifyContent:'center'
    },
    row:{
        width:'100%',
        alignItems:'center',
        justifyContent:'space-between' ,
        flexDirection:'row'   
    },
    text:{
        color:'#ffb14c',
        fontSize: 19
    }

})
