import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import {SmallButton,LargeButton} from '../../components/buttons'

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const globalStyles = require('../../styles/style')

export default function Home(props) {
    return (
        <View style={[globalStyles.container,styles.container]}>
            <View style={styles.top}>
                <SmallButton style={{backgroundColor:'#8EC63F', width: 150}} pressed={()=> props.navigation.navigate('WithdrawCoins')}>
                    <Text style={[globalStyles.text,{color:'white'}]}>Withdraw Coins</Text>
                </SmallButton>
                <View style={{ backgroundColor: 'white', width: 75,height:30, flexDirection:'row',alignItems:'center',borderRadius: 40,justifyContent:'space-around',paddingLeft:10,paddingRight:10}}>
                    <FontAwesome5 name={'coins'} size={17} color="#FFBB00" />
                    <Text style={[globalStyles.text, { color: '#FFBB00' }]}>100</Text>
                </View>
            </View>
            <View style={styles.middle}>
                <LargeButton pressed={() => props.navigation.navigate('StartGame')} title='START GAME' gradientColors={['#FFC04D', '#F57013', '#F05D07']}/>
                <LargeButton pressed={() => props.navigation.navigate('Settings')} title='SETTINGS' gradientColors={['#FFC04D', '#F57013', '#F05D07']}/>
                <LargeButton pressed={() => props.navigation.navigate('Store')}title='STORE' gradientColors={['#FEECBC', '#F69890', '#ED4465']}/>
            </View>
            <View style={styles.login}>
        <Text style={{color:'#FFBB00',fontWeight: '700'}}>LOGIN</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        padding: 20,
        paddingLeft: 35,
        paddingRight: 35,
        alignItems: 'center',
        justifyContent:'space-between',
    },
    top:{
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    middle:{
        width:'100%',
        height:'32%',
        alignItems:'center',
        justifyContent:'space-around',
        // borderWidth:1
    },
    login:{
        width: 50,
        height:50,
        alignItems:'center',
        justifyContent:'center',
        borderRadius: 25,
        alignSelf: 'flex-start',
        backgroundColor: 'white'
    }
})
