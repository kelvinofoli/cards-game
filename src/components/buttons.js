import React from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get('window')

function SmallButton(props) {
    return (
        <View style={[styles.smallButton, props.style]} onTouchEnd={() => props.pressed()}>
            {props.children ? props.children : <Text style={styles.title}>{props.title}</Text>}
        </View>
    )
}
function LargeButton(props) {
    return (
        <View >
            {props.children ? props.children :
                <LinearGradient style={[styles.largeButton, props.style]} colors={props.gradientColors} onTouchEnd={() => props.pressed()}>
                    <Text style={styles.largeText}>
                        {props.title}
                    </Text>
                </LinearGradient>
            }
        </View>
    )
}
function SwitchButton(props) {
    return (
        <View style={styles.switchButton}>
                <LinearGradient style={{alignItems:'center',justifyContent:'center',width:35,borderTopLeftRadius:15,borderBottomLeftRadius:15}} colors={['#FFC04D', '#F57013', '#F05D07']} onTouchEnd={() => props.pressed()}>
                    <Text style={styles.switchText}>
                        ON
                    </Text>
                </LinearGradient>
                <LinearGradient style={{alignItems:'center',justifyContent:'center',width:35,borderTopRightRadius:15,borderBottomRightRadius:15}} colors={['#e5e5e5', '#bfbfbf', '#848484']} onTouchEnd={() => props.pressed()}>
                    <Text style={styles.switchText}>
                        OFF
                    </Text>
                </LinearGradient>
        </View>
    )
}
const styles = StyleSheet.create({
    switchText:{
        fontSize: 12,
        color: 'white'
    },  
    switchButton:{
        width: 76,
        height: 25,
        flexDirection:'row',
        borderRadius: 15,
        borderWidth:3,
        borderColor:'#ffb14c'
    },
    smallButton: {
        width: 60,
        height: 30,
        borderRadius: 20,
        backgroundColor: '#6e91bf',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 6,
        // },
        // shadowOpacity: 0.2,
        // shadowRadius: 7.49,

        // elevation: 12,
    },
    title: {
        textAlign: 'center',
        color: 'white',
        fontSize: 17
    },
    largeButton: {
        width: width * 0.6,
        height: 50,
        borderRadius: 50,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 7.49,
        elevation: 12,
    },
    largeText:{
        color:'white',
        fontSize: 27
    }
})
export { SmallButton, LargeButton,SwitchButton }