"use strict";

var React = require("react-native");

var { StyleSheet } = React;

module.exports = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "#F25C05",
    width: '100%',
    height: '100%'
  },
  header: {
    fontSize: 17,
    color: "white",
    alignSelf: "flex-start",
    fontFamily: "Poppins-Regular"
  },
  text: {
    fontSize: 17,
    color: "black",
    // fontFamily: "Poppins-Regular",
    textAlign: 'center'
  },
  RouteHeader: {
    fontFamily: 'Poppins-SemiBold',
    color: 'white',
    fontSize: 20,
    marginRight: 15
  },
  headerStyles: {
    backgroundColor: '#d35100',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    elevation: 0,
  },
  button: {
    borderWidth: 1,
    borderColor: 'white',
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 25,
    backgroundColor: null
},
txt: {
  fontSize: 22,
  color: 'white'
}
});
