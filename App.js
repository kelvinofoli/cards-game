import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar
} from 'react-native';
import Routes from './src/routes/routes'
 
const App: () => React$Node = () => {
  return (
    <>
      <StatusBar hidden={true} />
      <SafeAreaView style={{...StyleSheet.absoluteFillObject}}>
        <Routes/>
      </SafeAreaView>
    </>
  );
};
export default App;
